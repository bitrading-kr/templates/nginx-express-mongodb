#!/bin/bash

# Install express
rm -rf express/node_modules
rm -f express/package-lock.json
npm install --prefix ./express

# Add .env variables
cp .env-example .env

# Deploy docker-compose
docker-compose up --build -d

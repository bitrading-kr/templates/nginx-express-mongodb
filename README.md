# nginx-backend-mariadb

Nginx-Backend-MariaDB tempalte
- DB Initialize script
- Install with ./Install.sh
- Needs nodejs
- Not supports submodule
- Use .env-example for .env

## Default backend modules
- https://gitlab.com:bitrading-kr/modules/bitrading-module-manager.git : v3.0

## mongodb
- MONGO_INITDB_DATABASE 인자를 ,(쉼표)로 구분하면 여러 DB를 초기화할 수 있다.
- db/common.js 는 여러 DB의 경우 모두 적용되는 스크립트이고 db/db1.js, db/db2.js 등은 각 db 이름(db1, db2) 에 적용되는 스크립트이다.

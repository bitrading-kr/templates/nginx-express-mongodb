const mongoose = require('mongoose');

class Db {
  constructor () {
  }

  async connect (host, port, user, password, db='admin') {
    let uri = `mongodb://${user}:${password}@${host}:${port}/${db}`;
    await mongoose.connect(uri);
  }
}

module.exports = Db;

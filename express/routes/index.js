var express = require('express');
var router = express.Router();
var Db = require('../middleware/Db');

/* GET home page. */
router.get('/', async function(req, res, next) {
  try {
    let db = new Db();
    await db.connect(process.env.DB_HOST, process.env.DB_PORT, process.env.DB_USERNAME, process.env.DB_PASSWORD, process.env.DB_DATABASE, { useNewUrlParser: true });
  } catch (e) {
    res.send("Mongo DB is not connected..." + e).end();
  }
});

module.exports = router;
